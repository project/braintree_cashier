<?php

namespace Drupal\braintree_cashier\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Billing plan entities.
 *
 * @ingroup braintree_cashier
 *
 * @deprecated This will be removed in the 8.4.x branch of Braintree Cashier.
 */
class BillingPlanDeleteForm extends ContentEntityDeleteForm {


}
