<?php

namespace Drupal\braintree_cashier;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for billing_plan.
 *
 * @deprecated This will be removed in the 8.4.x branch of Braintree Cashier.
 */
class BillingPlanTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
